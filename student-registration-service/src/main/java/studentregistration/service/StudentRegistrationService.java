package studentregistration.service;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import messaging.Event;
import messaging.MessageQueue;

public class StudentRegistrationService {

	public static final String STUDENT_REGISTRATION_REQUESTED = "StudentRegistrationRequested";
	public static final String STUDENT_ID_ASSIGNED = "StudentIdAssigned";
	private MessageQueue queue;
	private Map<CorrelationId, CompletableFuture<Student>> correlations = new ConcurrentHashMap<>();
	private static final Logger LOGGER = Logger.getLogger(StudentRegistrationService.class.getName());

	public StudentRegistrationService(MessageQueue q) {
		queue = q;
		queue.addHandler(STUDENT_ID_ASSIGNED, this::handleStudentIdAssigned);
	}

	public Student register(Student s) {
		var correlationId = CorrelationId.randomId();
		correlations.put(correlationId,new CompletableFuture<>());
		Event event = new Event(STUDENT_REGISTRATION_REQUESTED, new Object[] { s, correlationId });
		queue.publish(event);
		LOGGER.log(Level.INFO, "Hello world!" + correlations.get(correlationId).join());


		return correlations.get(correlationId).join();
	}

	public void handleStudentIdAssigned(Event e) {
		var s = e.getArgument(0, Student.class);
		var correlationid = e.getArgument(1, CorrelationId.class);
		correlations.get(correlationid).complete(s);
	}
}
